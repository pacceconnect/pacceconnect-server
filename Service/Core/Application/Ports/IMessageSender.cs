﻿using Application.Utils.BaseMessage;

namespace Application.Ports
{
    public interface IMessageSender
    {
        void SendMessage(BaseMessage message, string queueName);
    }
}
