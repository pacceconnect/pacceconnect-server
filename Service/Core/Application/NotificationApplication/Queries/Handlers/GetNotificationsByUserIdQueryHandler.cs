﻿using Application.NotificationApplication.Dto;
using Application.Utils;
using Application.Utils.ResponseBase;
using Domain.NotificationDomain.Ports;
using MediatR;

namespace Application.NotificationApplication.Queries.Handlers;

public class GetNotificationsByUserIdQueryHandler : IRequestHandler<GetNotificationsByUserIdQuery, Response>
{
    private readonly INotificationRepository _notificationRepository;

    public GetNotificationsByUserIdQueryHandler(INotificationRepository notificationRepository)
    {
        _notificationRepository = notificationRepository;
    }
    
    public async Task<Response> Handle(GetNotificationsByUserIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var notifications = await _notificationRepository.GetNotificationsByUserId(request.UserId);

            var notificationsDto = NotificationDto.MapToDto(notifications.ToList());

            return new Response.Success(notificationsDto);
        }
        catch (Exception)
        {
            return new Response.InternalServerError("Notifications could not be founded", ErrorCodes.NOTIFICATIONS_COULD_NOT_BE_FOUND);
        }
    }
}