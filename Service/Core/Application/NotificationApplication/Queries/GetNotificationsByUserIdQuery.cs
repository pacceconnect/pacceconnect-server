﻿using Application.Utils.ResponseBase;
using MediatR;

namespace Application.NotificationApplication.Queries;

public class GetNotificationsByUserIdQuery : IRequest<Response>
{
    public int UserId { get; set; }
    
    public GetNotificationsByUserIdQuery(int userId)
    {
        UserId = userId;
    }
}