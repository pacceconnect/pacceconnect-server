﻿using Application.Utils;
using Application.Utils.ResponseBase;
using Domain.NotificationDomain.Ports;
using MediatR;

namespace Application.NotificationApplication.Commands.Handlers;

public class ReadNotificationCommandHandler : IRequestHandler<ReadNotificationCommand, Response>
{
    private readonly INotificationRepository _notificationRepository;

    public ReadNotificationCommandHandler(INotificationRepository notificationRepository)
    {
        _notificationRepository = notificationRepository;
    }

    public async Task<Response> Handle(ReadNotificationCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var notification = await _notificationRepository.GetNotificationById(request.NotificationId);

            notification.Read();

            _notificationRepository.Save(notification);

            return new Response.Success();
        }
        catch (Exception)
        {
            return new Response.InternalServerError("Notification could not be read", ErrorCodes.NOTIFICATION_COULD_NOT_BE_READ);
        }
    }
}