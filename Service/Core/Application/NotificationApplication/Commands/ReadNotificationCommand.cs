﻿using Application.Utils.ResponseBase;
using MediatR;

namespace Application.NotificationApplication.Commands;

public class ReadNotificationCommand : IRequest<Response>
{
    public int NotificationId { get; set; }
    
    public ReadNotificationCommand(int notificationId)
    {
        NotificationId = notificationId;
    }
    
}