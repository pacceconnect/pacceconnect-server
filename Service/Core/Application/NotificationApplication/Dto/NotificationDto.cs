﻿using Application.Utils.IDtoBase;
using Domain.CellDomain.Enuns;
using Domain.NotificationDomain;

namespace Application.NotificationApplication.Dto;

public class NotificationDto : IDto
{
    public int Id { get; set; }
    public string CellName { get; set; } = string.Empty;
    public StatusCell CellStatus { get; set; }
    public DateTime CreatedAt { get; set; }
    public bool IsRead { get; set; }
    
    public static NotificationDto MapToDto(Notification notification)
    {
        return new NotificationDto
        {
            Id = notification.Id,
            CellName = notification.CellName,
            CellStatus = notification.CellStatus,
            CreatedAt = notification.CreatedDate,
            IsRead = notification.IsRead
        };
    }
    
    public static List<NotificationDto> MapToDto(List<Notification> notifications)
    {
        return notifications.Select(MapToDto).ToList();
    }
}