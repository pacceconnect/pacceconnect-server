﻿namespace Application.Utils.BaseMessage;

public class BaseMessage
{
    public DateTime CreatedDate { get; set; }
    
    public BaseMessage()
    {
        CreatedDate = DateTime.Now;
    }
}