﻿using Application.Utils.BaseMessage;
using Domain.CellDomain.Enuns;

namespace Application.CellApplication.Dtos;

public class CellStatusNotificationDto : BaseMessage
{
    public int Id { get; set; }
    public int ArticulatorId { get; set; }
    public int CellId { get; set; }
    public string CellName { get; set; }
    public StatusCell CellStatus { get; set; }
}