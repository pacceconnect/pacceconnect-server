﻿using Application.CellApplication.Dtos;
using Application.Ports;
using Application.Utils;
using Application.Utils.ResponseBase;
using Domain.CellDomain.Entities;
using Domain.CellDomain.Enuns;
using Domain.CellDomain.Ports;
using MediatR;
using static Application.Utils.ResponseBase.Response;
using Action = Domain.CellDomain.Enuns.Action;

namespace Application.CellApplication.Commands.Handlers
{
    public class UpdateStatusCellCommandHandler : IRequestHandler<UpdateStatusCellCommand, Response>
    {
        private readonly ICellRepository _cellRepository;
        private readonly IMessageSender _messageSender;

        public UpdateStatusCellCommandHandler(
            ICellRepository cellRepository,
            IMessageSender messageSender)
        {
            _cellRepository = cellRepository;
            _messageSender = messageSender;
        }

        public async Task<Response> Handle(UpdateStatusCellCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var cellId = request.CellId;
                var action = request.Action;

                var cell = await _cellRepository.GetCellById(cellId);
                if (cell == null)
                {
                    return new BadRequest("Cell not found", ErrorCodes.CELL_NOT_FOUND);
                }

                if (action == Action.Approve && !cell.CellPlan.PlanIsCompleted())
                {
                    return new BadRequest("Unable to update status", ErrorCodes.CELL_CHANGE_STATE_NOT_IS_POSSIBLE);
                }

                cell.ChangeState(action);

                await cell.Save(_cellRepository);
                
                if (cell.Status == StatusCell.Active) { SendNotificationByCellStatus(cell); }
                else if (cell.Status == StatusCell.Reviewed) { SendNotificationByCellStatus(cell); }
                
                return new Success();
            }
            catch (Exception)
            {
                return new BadRequest("Unable to update status", ErrorCodes.CELL_CHANGE_STATE_NOT_IS_POSSIBLE);
            }

        }
        
        private void SendNotificationByCellStatus(Cell cell)
        {
            var cellStatusNotificationDto = new CellStatusNotificationDto
            {
                ArticulatorId = cell.ArticulatorId,
                CellId = cell.Id,
                CellName = cell.Name,
                CellStatus = cell.Status
            };
            
            _messageSender.SendMessage(cellStatusNotificationDto, "cellStatusNotificationQueue");
        }
    }
}
