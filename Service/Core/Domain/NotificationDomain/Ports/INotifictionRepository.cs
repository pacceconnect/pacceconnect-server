﻿namespace Domain.NotificationDomain.Ports;

public interface INotificationRepository
{
    public Task<IEnumerable<Notification>> GetNotificationsByUserId(int articulatorId);
    public Task<Notification> GetNotificationById(int notificationId);
    public void Save(Notification notification);
}