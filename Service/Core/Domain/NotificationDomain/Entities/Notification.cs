﻿using Domain.CellDomain.Enuns;
using Domain.NotificationDomain.Enuns;

namespace Domain.NotificationDomain;

public class Notification
{
    public Notification()
    {
        IsRead = false;
    }

    public int Id { get; set; }
    public int ArticulatorId { get; set; }
    public int CellId { get; set; }
    public string CellName { get; set; }
    public StatusCell CellStatus { get; set; }
    public NotificationType NotificationType { get; set; }
    public DateTime CreatedDate { get; set; }
    public Boolean IsRead { get; set; }
    
    public void Read()
    {
        IsRead = true;
    }
}