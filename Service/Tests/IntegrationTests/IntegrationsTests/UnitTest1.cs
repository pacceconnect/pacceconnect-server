using System.Text.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using NUnit.Framework;

namespace IntegrationsTests;

public class Tests
{
    private RestClient client;
    private RestRequest endpoint;
    private IRestResponse resp;
    public string name, status;
    public JArray photoUrls;
    
    public RestClient Client(string uri)
    {
        client = new RestClient(uri);
        return client;
    }
    
    public RestRequest Endpoint(string route)
    {
        endpoint = new RestRequest(route);
        return endpoint;
    }

    public void Get()
    {
        endpoint.Method = Method.GET;
        endpoint.RequestFormat = DataFormat.Json;
    }
    
    public void Post()
    {
        endpoint.Method = Method.POST;
        endpoint.RequestFormat = DataFormat.Json;
    }

    public void Body_json(string _body)
    {
        endpoint.AddParameter("application/json", _body, ParameterType.RequestBody);
    }

    public IRestResponse StatusCode(int code)
    {
       resp = client.Execute(endpoint);

        if (resp.IsSuccessful)
        {
            var status = (int)resp.StatusCode;
            Assert.AreEqual(code, status);
        }
        else
        {
            var status = (int)resp.StatusCode;
            var desc = resp.StatusDescription;
            var content = resp.Content;

            Console.WriteLine($"{status} - {desc}");
            Console.WriteLine(content);
            Assert.AreEqual(code, status);
        }

        return resp;
    }

    public void ReturnText()
    {
        var obs = resp.Content;
        Console.WriteLine(obs);
    }

    public string _json()
    {
        var body = @"{
            ""id"": 12345,
            ""category"": {
                ""id"": 1,
                ""name"": ""dog""
            },
            ""name"": ""Bob construtor"",
            ""photoUrls"": [
                ""algum link""
            ],
            ""tags"": [
                {
                    ""id"": 1,
                    ""name"": ""string""
                }
            ],
            ""status"": ""Em adoção""
        }";

        return body;
    }

    public dynamic GetValue(dynamic key)
    {
        dynamic obj = JProperty.Parse(resp.Content);

        var value = obj[key];
        return value;
    }
    
    [Test]
    public void GetById()
    {
        Client("https://petstore3.swagger.io");
        Endpoint("/api/v3/pet/12345");
        Get();
        StatusCode(200);
        ReturnText();
        
        string name_get = GetValue("name");
        Assert.AreEqual(name, name_get);
        
        JArray photoUrls_get = GetValue("photoUrls");
        Assert.AreEqual(photoUrls, photoUrls_get);
        
        string status_get = GetValue("status");
        Assert.AreEqual(status, status_get);
    }

    [Test]
    public void Cadaster()
    {
        Client("https://petstore3.swagger.io");
        Endpoint("/api/v3/pet");
        Post();
        Body_json(_json());
        StatusCode(200);
        ReturnText();
        
        name = GetValue("name");
        photoUrls = GetValue("photoUrls");
        status = GetValue("status");
    }

    [Test]
    public void CadasterValidate()
    {
        Cadaster();
        GetById();
    }
}