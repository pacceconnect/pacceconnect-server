﻿using Domain.NotificationDomain;
using Domain.NotificationDomain.Ports;
using Microsoft.EntityFrameworkCore;

namespace Data.NotificationData;

public class NotificationRepository : INotificationRepository
{
    private readonly PACCEConnectDbContext _context;

    public NotificationRepository(PACCEConnectDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Notification>> GetNotificationsByUserId(int userId)
    {
        return await _context.Notifications
            .Where(x => x.ArticulatorId == userId)
            .ToListAsync();
    }

    public async Task<Notification> GetNotificationById(int notificationId)
    {
        return await _context.Notifications
            .FirstOrDefaultAsync(x => x.Id == notificationId);
    }

    public void Save(Notification notification)
    {
        _context.Notifications.Update(notification);
        _context.SaveChanges();
    }
}