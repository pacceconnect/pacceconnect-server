CREATE TABLE `Notifications` (
    `Id` INT(10) NOT NULL AUTO_INCREMENT,
    `ArticulatorId` INT(10) NOT NULL,
    `NotificationType` INT(10) NOT NULL,
    `CellStatus` INT(10) NOT NULL,
    `CellId` INT(10) NOT NULL,
    `CellName` VARCHAR(50) NOT NULL,
    `CreatedDate` DATETIME NOT NULL,
    `IsRead` TINYINT(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`Id`),
    INDEX `FK_Notification_Articulator` (`ArticulatorId`),
    CONSTRAINT `FK_Notification_Articulator` FOREIGN KEY (`ArticulatorId`) REFERENCES `Articulators` (`Id`) ,
    INDEX `FK_Notification_Cell` (`CellId`),
    CONSTRAINT `FK_Notification_Cell` FOREIGN KEY (`CellId`) REFERENCES `Cells` (`Id`)
);