﻿using Application.Utils.BaseMessage;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;
using Application.CellApplication.Dtos;
using Application.Ports;
using Messenger.RabbitConfig;

namespace Mensseger.MessagerSender
{
    public class RabbitMQMessageSender : IMessageSender
    {
        private readonly RabbitMQOptions _options;
        private IConnection _connection;

        public RabbitMQMessageSender(IOptions<RabbitMQOptions> options)
        {
            _options = options.Value;
        }

        public void SendMessage(BaseMessage message, string queueName)
        {
            try
            {
                var factory = new ConnectionFactory
                {
                    HostName = _options.HostName,
                    UserName = _options.UserName,
                    Password = _options.Password,
                };
                _connection = factory.CreateConnection();

                using var channel = _connection.CreateModel();
                channel.QueueDeclare(queue: queueName,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);
                byte[] body = GetMessageAsByteArray(message);
                channel.BasicPublish(
                    exchange: "",
                    routingKey: queueName,
                    basicProperties: null,
                    body: body);
            }
            catch (Exception e)
            {
                return;
            }
            
            return;
        }

        private byte[] GetMessageAsByteArray(BaseMessage message)
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
            };

            var json = JsonSerializer.Serialize((CellStatusNotificationDto)message, options);
            var body = Encoding.UTF8.GetBytes(json);
            return body;
        }
    }
}