﻿using System.Text;
using System.Text.Json;
using MessengerConsumer.Model;
using MessengerConsumer.Options;
using MessengerConsumer.Repository.IRepository;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace MessengerConsumer.Service;

public class WorkerConsumer : BackgroundService
{
    private readonly ILogger<WorkerConsumer> _logger;
    private readonly RabbitMQOptions _options;
    private readonly ICellStatusRepository _cellStatusRepository;
    private IConnection _connection;
    private IModel _channel;
    
    public WorkerConsumer(
        ILogger<WorkerConsumer> logger, 
        IOptions<RabbitMQOptions> options,
        ICellStatusRepository cellStatusRepository)
    {
        _logger = logger;
        _options = options.Value;
        _cellStatusRepository = cellStatusRepository;
        var factory = new ConnectionFactory
        {
            HostName = _options.HostName,
            UserName = _options.UserName,
            Password = _options.Password,
        };
        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
        _channel.QueueDeclare(queue: "cellStatusNotificationQueue",
            durable: false,
            exclusive: false,
            autoDelete: false,
            arguments: null);
    }
    
    

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        stoppingToken.ThrowIfCancellationRequested();

        var consumer = new EventingBasicConsumer(_channel);
        consumer.Received += (ch, ea) =>
        {
            var content = Encoding.UTF8.GetString(ea.Body.ToArray());
            var cellStatusNotification = JsonSerializer.Deserialize<CellStatusNotification>(content);
            _cellStatusRepository.Create(cellStatusNotification);
            _channel.BasicAck(ea.DeliveryTag, false);
        };
        _channel.BasicConsume("cellStatusNotificationQueue", false, consumer);
        return Task.CompletedTask;
    }
}