﻿using MessengerConsumer.Model.Enuns;
using Type = System.Type;

namespace MessengerConsumer.Model;

public class CellStatusNotification
{
    public int Id { get; set; }
    public int ArticulatorId { get; set; }
    public int CellId { get; set; }
    public string CellName { get; set; }
    public StatusCell CellStatus { get; set; }
    public DateTime CreatedDate { get; set; }
    public NotificationType NotificationType { get; set; } = NotificationType.PopUpAndEmail;
    public Boolean IsRead { get; set; }
}