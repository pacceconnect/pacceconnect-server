﻿namespace MessengerConsumer.Model.Enuns;

public enum NotificationType
{
    PopUp = 1,
    Email = 2,
    PopUpAndEmail = 3
}