﻿using Microsoft.EntityFrameworkCore;

namespace MessengerConsumer.Model;

public class PACCEConnectDbContext : DbContext
{
    public PACCEConnectDbContext(DbContextOptions<PACCEConnectDbContext> options) : base(options) {}

    public virtual DbSet<CellStatusNotification> Notifications { get; set; }
}