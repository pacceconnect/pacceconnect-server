﻿using MessengerConsumer.Model;

namespace MessengerConsumer.Repository.IRepository;

public interface ICellStatusRepository
{
    Task Create(CellStatusNotification notification);
}