﻿using MessengerConsumer.Model;
using MessengerConsumer.Repository.IRepository;
using Microsoft.EntityFrameworkCore;

namespace MessengerConsumer.Repository;

public class CellStatuRepository : ICellStatusRepository
{
    private readonly PACCEConnectDbContext _context;
    
    public CellStatuRepository(PACCEConnectDbContext context)
    {
        _context = context;
    }

    public Task Create(CellStatusNotification notification)
    {
        _context.Notifications.Add(notification);
        return _context.SaveChangesAsync();
    }
}