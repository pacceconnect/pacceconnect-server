using MessengerConsumer;
using MessengerConsumer.Model;
using MessengerConsumer.Options;
using MessengerConsumer.Repository;
using MessengerConsumer.Repository.IRepository;
using MessengerConsumer.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;

var builder = WebApplication.CreateBuilder(args);

// Add Configurare Options
#region
builder.Services.Configure<RabbitMQOptions>(builder.Configuration.GetSection("RabbitMQ"));
#endregion

// Add Connection Database
#region
var connectionString = builder.Configuration["ConnectionStrings:MySQLConnectionStringDocker"];
var optionsBuilder = new DbContextOptionsBuilder<PACCEConnectDbContext>();
optionsBuilder.UseMySql(connectionString, new MySqlServerVersion(new Version(8, 0, 5)));
#endregion

builder.Services.AddDbContext<PACCEConnectDbContext>(options =>
    options.UseMySql(connectionString, new MySqlServerVersion(new Version(8, 0, 5))));

builder.Services.AddSingleton<ICellStatusRepository, CellStatuRepository>();

// Add Worker
builder.Services.AddHostedService<WorkerConsumer>();

var app = builder.Build();

app.Run();