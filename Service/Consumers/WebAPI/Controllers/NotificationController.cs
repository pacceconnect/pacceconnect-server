﻿using Application.NotificationApplication.Commands;
using Application.NotificationApplication.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class NotificationController : ControllerBase 
{
    private readonly IMediator _mediator;

    public NotificationController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    [HttpGet("{userId}")]
    public async Task<IActionResult> GetNotificationsByUserId(int userId)
    {
        if (!this.ModelState.IsValid)
        {
            return this.BadRequest(new ValidationProblemDetails(this.ModelState));
        }

        var handlerReponse = await _mediator
            .Send(new GetNotificationsByUserIdQuery(userId))
            .ConfigureAwait(false);

        return handlerReponse.Match<ActionResult>(
            Success => this.Ok(Success.Dtos),
            BadRequest =>
            {
                this.ModelState.AddModelError("Message", BadRequest.Message);
                this.ModelState.AddModelError("ErrorCode", $"{BadRequest.ErrorCodes}");

                return this.BadRequest(new ValidationProblemDetails(this.ModelState));
            },
            InternalServerError =>
            {
                this.ModelState.AddModelError("Message", InternalServerError.Message);
                this.ModelState.AddModelError("ErrorCode", $"{InternalServerError.ErrorCodes}");

                return this.NotFound(new ValidationProblemDetails(this.ModelState));
            });
    }
    
    [HttpPut("{notificationId}/read")]
    public async Task<IActionResult> ReadNotification(int notificationId)
    {
        if (!this.ModelState.IsValid)
        {
            return this.BadRequest(new ValidationProblemDetails(this.ModelState));
        }

        var handlerReponse = await _mediator
            .Send(new ReadNotificationCommand(notificationId))
            .ConfigureAwait(false);

        return handlerReponse.Match<ActionResult>(
            Success => this.Ok(Success),
            BadRequest =>
            {
                this.ModelState.AddModelError("Message", BadRequest.Message);
                this.ModelState.AddModelError("ErrorCode", $"{BadRequest.ErrorCodes}");

                return this.BadRequest(new ValidationProblemDetails(this.ModelState));
            },
            InternalServerError =>
            {
                this.ModelState.AddModelError("Message", InternalServerError.Message);
                this.ModelState.AddModelError("ErrorCode", $"{InternalServerError.ErrorCodes}");

                return this.NotFound(new ValidationProblemDetails(this.ModelState));
            });
    }
}